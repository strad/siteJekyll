---
layout: post
title: "Conception de ce site"
date: 2024-10-15
---

Ce site web a été réalisé en quelques minutes avec le générateur de site statique Jekyll à partir du thème [console](https://github.com/b2a3e8/jekyll-theme-console).